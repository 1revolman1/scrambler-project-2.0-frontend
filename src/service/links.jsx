export const backend_url = process.env.REACT_APP_BACKENDLINK;
//----------------------------HOME-----------------------
export const home = `${backend_url}/api/home`;

//----------------------------TORRENT---------------------
export const torrent = `${backend_url}/api/torrent`;
export const torrent_film = `${backend_url}/api/torrent/film`;
//---------------------------SIDEBAR-----------------------
export const sidebar = `${backend_url}/api/home/sidebar`;
// /api/aceikmnn / kholmetskaya;
//---------------------------NICKNAME-----------------------
export const nickname = `${backend_url}/api/nickname`;
