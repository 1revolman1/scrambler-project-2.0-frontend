import React, { useEffect, useMemo } from "react";
import { StyledNewWrapper, Loader } from "./styled";
import { useDispatch, useSelector } from "react-redux";
import {
  torrentGetInfoAboutMyTorrents,
  torrentGetInformationAboutFilmById,
} from "../../redux/actions/Torrent";
import { sidebarOpenRightPanel } from "../../redux/actions/Sidebar";
import {
  isTorrentDataLoading,
  torrent_data,
} from "../../redux/selector/Torrent";
import Table from "../../components/LeagueTable";
import IpInfo from "./components/IpInfo";

export default function Torrent() {
  //REDUX
  const dispatch = useDispatch();
  const isLoading = useSelector(isTorrentDataLoading);
  const torrentData = useSelector(torrent_data);
  //REDUX
  const getInformationAboutFilm = function ({ id }) {
    dispatch(sidebarOpenRightPanel(true));
    dispatch(torrentGetInformationAboutFilmById(id));
    console.log("ID", id);
  };
  useEffect(() => {
    dispatch(torrentGetInfoAboutMyTorrents());
  }, []);

  const columns = useMemo(
    () => [
      {
        Header: "Имя",
        accessor: "name",
      },
      {
        Header: "Размер",
        accessor: "size",
      },
      {
        Header: "Последняя информация",
        accessor: "lastData",
      },
      {
        Header: "Тип",
        accessor: "type",
      },
    ],
    []
  );

  if (isLoading)
    return <Loader type="Bars" color="#00BFFF" height={100} width={100} />;
  else
    return (
      <StyledNewWrapper>
        {torrentData && <IpInfo data={torrentData} />}
        {torrentData &&
        torrentData.content &&
        torrentData.content.length > 0 ? (
          <Table
            columns={columns}
            canSelectPlayer={true}
            data={torrentData.content}
            getInformationAboutFilm={getInformationAboutFilm}
          />
        ) : (
          <Table
            columns={[
              {
                Header: "Имя",
                accessor: "name",
              },
            ]}
            data={[{ name: "Пусто" }]}
          />
        )}
      </StyledNewWrapper>
    );
}
