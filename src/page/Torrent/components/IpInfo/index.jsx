import React from "react";
import {
  StyledContainer,
  StyledBlock,
  StyledLeftOpener,
  StyledArrow,
} from "./styled";
import { useSelector, useDispatch } from "react-redux";
import { dataForSidebar } from "../../../../redux/selector/Sidebar";
import { sidebarOpenLeftPanel } from "../../../../redux/actions/Sidebar";

export default function IpInfo({
  data: {
    ip,
    country,
    continent,
    city,
    org,
    hasChildPornography,
    hasPornography,
  },
}) {
  const dispatch = useDispatch();
  const sidebar = useSelector(dataForSidebar);
  return (
    <>
      <h1>
        {ip && sidebar && ip === sidebar.ip
          ? "Информация про ваши торренты"
          : `Информация о торрентах адресса - ${ip} `}
      </h1>
      <StyledContainer>
        {ip && (
          <StyledBlock>
            <p>{ip}</p>
          </StyledBlock>
        )}
        {continent && (
          <StyledBlock>
            <p>{continent}</p>
          </StyledBlock>
        )}
        {country && (
          <StyledBlock>
            <p>{country}</p>
          </StyledBlock>
        )}
        {city && (
          <StyledBlock>
            <p>{city}</p>
          </StyledBlock>
        )}
        {org && (
          <StyledBlock>
            <p>{org}</p>
          </StyledBlock>
        )}
        {hasPornography && (
          <StyledBlock data-type="porn">
            <p>Скачивают порно</p>
          </StyledBlock>
        )}
        {hasChildPornography && (
          <StyledBlock data-type="cp">
            <p>Скачивают запрещённый контент</p>
          </StyledBlock>
        )}
      </StyledContainer>
      <StyledLeftOpener onClick={() => dispatch(sidebarOpenLeftPanel(true))}>
        <p>Проверить конкретный IP</p>
        <StyledArrow />
      </StyledLeftOpener>
    </>
  );
}
