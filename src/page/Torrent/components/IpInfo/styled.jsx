import styled from "styled-components";
import { ReactComponent as Arrow } from "./assets/Arrow.svg";

export const StyledArrow = styled(Arrow)`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translate(0, -50%);
  cursor: pointer;
`;
export const StyledLeftOpener = styled.div`
  position: relative;
  background: #ffffff;
  border: 1px solid #d1d1d1;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
  border-radius: 5px;
  padding: 27px 24px;
  margin-bottom: 40px;
  @media screen and (min-width: 1101px) {
    display: none;
  }
  p {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14.222px;
    line-height: 16px;
    letter-spacing: 0.005em;
    color: #000000;
  }
`;

export const StyledContainer = styled.section`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 24px;
`;

export const StyledBlock = styled.div`
  margin: 10px;
  background: var(--main-active-color);
  padding: 8px;
  border-radius: 10px;
  @media screen and (max-width: 450px) {
    flex: 1 30%;
  }
  @media screen and (max-width: 350px) {
    flex: 1 61%;
    margin: 10px 0;
  }
  &[data-type="porn"] {
    background: #e1a9a9;
  }
  &[data-type="cp"] {
    background: red;
  }
  p {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: white;
  }
`;
