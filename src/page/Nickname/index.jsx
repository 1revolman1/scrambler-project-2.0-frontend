import React, { useEffect } from "react";
import {
  StyledNewWrapper,
  StyledBlockContainer,
  StyledNicknameBlock,
  Loader,
  StyledLeftOpener,
  StyledArrow,
} from "./styled";
import { sidebarOpenLeftPanel } from "../../redux/actions/Sidebar";
import { nicknameGetInformationAboutServices } from "../../redux/actions/Nickname";
import {
  nicknameService,
  isNicknameServiceLoading,
  isSelectedNicknameLoading,
} from "../../redux/selector/Nickname";

import { useDispatch, useSelector } from "react-redux";

export default function Nickname() {
  //REDUX
  const dispatch = useDispatch();
  const service = useSelector(nicknameService);
  const isLoading = useSelector(isNicknameServiceLoading);
  const isNicknameLoading = useSelector(isSelectedNicknameLoading);
  //REDUX
  useEffect(() => {
    dispatch(nicknameGetInformationAboutServices());
  }, []);
  if (isLoading || isNicknameLoading)
    return <Loader type="Bars" color="#00BFFF" height={100} width={100} />;
  else
    return (
      <StyledNewWrapper>
        <h1>Информация про никнейм</h1>
        <StyledLeftOpener onClick={() => dispatch(sidebarOpenLeftPanel(true))}>
          <p>Проверить никнейм</p>
          <StyledArrow />
        </StyledLeftOpener>
        <StyledBlockContainer>
          {service.length > 0 &&
            service.map(({ service, available, url }, index) => {
              return (
                <StyledNicknameBlock
                  data-founded={typeof available === "boolean" && !available}
                  key={`${index}+${service}`}
                >
                  {url && <a href={url} className="link"></a>}
                  <h3>{service}</h3>
                  <p>
                    {typeof available === "boolean" && !available && "Найден!"}
                    {typeof available === "boolean" && available && "Пусто"}
                  </p>
                </StyledNicknameBlock>
              );
            })}
        </StyledBlockContainer>
      </StyledNewWrapper>
    );
}
