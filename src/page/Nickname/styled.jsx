import styled from "styled-components";
import { StyledWrapper, StyledLoader } from "../styled";
import { ReactComponent as Arrow } from "./assets/Arrow.svg";

export const StyledArrow = styled(Arrow)`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translate(0, -50%);
  cursor: pointer;
`;
export const StyledLeftOpener = styled.div`
  position: relative;
  background: #ffffff;
  border: 1px solid #d1d1d1;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
  border-radius: 5px;
  padding: 27px 24px;
  margin-bottom: 40px;
  @media screen and (min-width: 1101px) {
    display: none;
  }
  p {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14.222px;
    line-height: 16px;
    letter-spacing: 0.005em;
    color: #000000;
  }
`;

export const Loader = styled(StyledLoader)`
  position: absolute;
`;

export const StyledNicknameBlock = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding: 20px;
  border-radius: 15px;
  border: 3px dashed var(--main-active-color);
  background: white;
  flex: 1 25%;
  margin: 15px 25px;
  .link {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  &[data-founded="true"] {
    background: var(--main-active-color);
    border: 3px dashed white;
    cursor: pointer;
    h3,
    p {
      color: white;
    }
    &:hover {
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
    }
  }
  h3 {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 100%;
    letter-spacing: 0.01em;
    color: #000000;
    text-align: center;
  }
  p {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 100%;
    letter-spacing: 0.01em;
    color: #000000;
    text-align: center;
  }
`;

export const StyledNewWrapper = styled(StyledWrapper)`
  flex-direction: column;
  h1 {
    width: fit-content;
  }
  .team-name {
    display: flex;
    align-items: center;
    flex-direction: row;
    margin-bottom: 24px;
    @media (min-width: 48em) {
      margin: 0 25px;
      margin-bottom: 24px;
    }
    @media screen and (max-width: 768px) {
      margin: 0 0 24px 0;
    }
    @media screen and (max-width: 426px) {
      margin: 15px 0 24px 0;
    }
    @media screen and (min-width: 769px) {
      margin: 0 20px 25px;
    }
    > img {
      width: 50px;
      height: 50px;
      object-fit: cover;
      margin-right: 15px;
    }
    > h1 {
      margin: 0;
      font-family: "Gilroy", sans-serif;
      font-style: normal;
      font-weight: 900;
      font-size: 28.833px;
      line-height: 41px;
      width: 100%;
      text-transform: none;

      width: auto;
      @media screen and (max-width: 426px) {
        margin: 0;
      }
      @media screen and (max-width: 768px) {
        margin: 0;
      }
      @media screen and (min-width: 769px) {
      }
    }
  }
`;
export const StyledBlockContainer = styled.section`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;
