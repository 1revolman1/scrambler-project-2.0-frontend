import React from "react";
import { StyledBlock, StyledContainer, StyledTorrentBlock } from "./styled";
import { useDispatch } from "react-redux";
import { sidebarOpenRightPanel } from "../../../../redux/actions/Sidebar";
import { homeSelectFilmById } from "../../../../redux/actions/Home";

export default function LastKnownTorrent({ data }) {
  //REDUX
  const dispatch = useDispatch();
  //REDUX
  const openAdditionalInfo = function () {
    dispatch(sidebarOpenRightPanel(true));
    dispatch(homeSelectFilmById(data.id));
  };
  return (
    <StyledContainer>
      <h2>Последний торрент по вашему IP:</h2>
      <StyledBlock>
        {data.type && <h3>{data.type}</h3>}
        <StyledTorrentBlock>
          <ul>
            {data.name && (
              <li>
                <span>Имя:</span> {data.name}
              </li>
            )}
            {data.size && (
              <li>
                <span>Размер:</span> {data.size}
              </li>
            )}
            {data.lastData && (
              <li>
                <span>Дата:</span> {data.lastData}
              </li>
            )}
          </ul>
        </StyledTorrentBlock>
        {data.type && (data.type === "Фильм" || data.type === "Series") && (
          <p onClick={openAdditionalInfo}>Кликни, что бы узнать больше....</p>
        )}
      </StyledBlock>
    </StyledContainer>
  );
}
