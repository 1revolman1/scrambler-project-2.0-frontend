import React from "react";
import { StyledContainer, StyledList, StyledLoader, StyledImg } from "./styled";
import { useSelector } from "react-redux";
import {
  isFilmLoaded,
  dataOfSelectedFilm,
} from "../../../../../../redux/selector/Torrent";
export default React.memo(function TorrentSidebar() {
  //REDUX
  const isLoad = useSelector(isFilmLoaded);
  const data = useSelector(dataOfSelectedFilm);
  //REDUX

  if (isLoad) {
    return (
      <StyledLoader type="Bars" color="#00BFFF" height={100} width={100} />
    );
  }
  return (
    <StyledContainer>
      <h3>Информация про кино: </h3>
      {data && (
        <>
          {data.photo && (
            <StyledImg>
              <img src={data.photo} alt="Icon For Film" />
            </StyledImg>
          )}
          {data.data && (
            <StyledList>
              {data.data.map(({ first, second }, index) => {
                return (
                  <li key={index}>
                    {first === "Рейтинги" ? (
                      <>
                        <span>{first}</span> <img src={second} alt="" />
                      </>
                    ) : (
                      <>
                        <span>{first}</span> {second}
                      </>
                    )}
                  </li>
                );
              })}
            </StyledList>
          )}
        </>
      )}
    </StyledContainer>
  );
});
