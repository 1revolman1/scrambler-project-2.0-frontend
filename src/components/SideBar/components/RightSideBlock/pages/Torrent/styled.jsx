import styled from "styled-components";
import Loader from "react-loader-spinner";

export const StyledLoader = styled(Loader)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
export const StyledContainer = styled.section`
  h3 {
    text-align: center;
  }
`;
export const StyledImg = styled.div`
  margin: 10px 0;
  img {
    object-fit: cover;
    width: 100%;
    border: 1px solid #d1d1d1;
    border-radius: 5px;
  }
`;
export const StyledList = styled.ul`
  border: 1px solid #d1d1d1;
  border-radius: 5px;
  padding: 11px 9px;
  li {
    list-style: none;
    margin: 5px 0;
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    span {
      font-family: "Gilroy", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 16px;
      line-height: 24px;
      color: #000000;
      margin-right: 5px;
    }
  }
`;
