import React from "react";

import { StyledHeader, StyledIp } from "./styled";
import Cloaser from "../Cloaser";
import { useSelector } from "react-redux";
import { dataForSidebar } from "../../../../redux/selector/Sidebar";

export default React.memo(function Header({
  location,
  openleftpanel,
  isleftopen,
  ...props
}) {
  const data = useSelector(dataForSidebar);

  return (
    <StyledHeader>
      <Cloaser {...props} />
      {data && data.ip && (
        <StyledIp>
          <p>
            <span>Ваш IP: </span>
            {`${data.ip}`}
          </p>
        </StyledIp>
      )}
    </StyledHeader>
  );
});
