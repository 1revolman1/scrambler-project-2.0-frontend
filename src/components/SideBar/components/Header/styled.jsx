import styled from "styled-components";

export const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  position: fixed;
  z-index: 4;
  width: 100%;
  padding: 19px 36px 19px 36px;
  min-height: 90px;
  background: #ffffff;
  border: 1px solid #e9e9e9;
  @media screen and (max-width: 1100px) {
    padding: 10px 24px 10px 36px;
    justify-content: space-between;
  }
  @media screen and (max-width: 426px) {
    min-height: 70px;
    padding: 19px 24px 19px 36px;
  }
`;

export const StyledIp = styled.div`
  background: #ffffff;
  border: 1px solid #000000;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
  border-radius: 5px;
  padding: 10px 18px;
  p {
    font-family: "Gilroy", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 20px;
    letter-spacing: -0.001em;
    color: #000000;
    /* span {
      @media screen and (max-width: 450px) {
        display: none;
      }
    } */
  }
`;
