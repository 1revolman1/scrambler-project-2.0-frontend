import React from "react";
import { ReactComponent as Lobby } from "../../assets/Lobby.svg";
import { ReactComponent as Team } from "../../assets/Team.svg";
import { ReactComponent as Torrent } from "../../assets/Torrent1.svg";
import { ReactComponent as Arrow } from "../../../../assets/img/SimpleArrow.svg";
import { StyledSideNav, StyledSummary, StyledNavLinks } from "./styled";

export default React.memo(function Menu({
  isOnMain,
  isSideOpen,
  isLeftOpen,
  openLeftPanel,
}) {
  return (
    <StyledSideNav data-left={isLeftOpen} data-sideopen={isSideOpen}>
      <StyledSummary
        data-left={isLeftOpen}
        className={isOnMain ? "isOnMain" : ""}
        onClick={openLeftPanel}
      >
        <p>Навигация</p>
        <Arrow />
      </StyledSummary>
      <StyledNavLinks
        exact
        to="/"
        activeClassName="selected"
        href="#"
        data-content="Лобби"
      >
        <Lobby />
        <span>Лобби</span>
      </StyledNavLinks>
      <StyledNavLinks
        activeClassName="selected"
        to="/nickname"
        href="#"
        data-content="Никнейм"
      >
        <Team />
        <span>Никнейм</span>
      </StyledNavLinks>
      <StyledNavLinks
        activeClassName="selected"
        to="/torrent"
        href="#"
        data-content="Торрент"
      >
        <Torrent data-type="torrent" />
        <span>Торрент</span>
      </StyledNavLinks>
    </StyledSideNav>
  );
});
