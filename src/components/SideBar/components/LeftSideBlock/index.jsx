import React from "react";
import { Switch, Route } from "react-router-dom";
import {
  StyledRightSideBlock,
  StyledArrow,
  StyledRelativeContainer,
} from "./styled";

import TorrentSide from "./pages/Torrent";
import NicknameSide from "./pages/Nickname";

export default React.memo(function LeftSideBlock({ leftPanel, openLeftPanel }) {
  return (
    <StyledRightSideBlock data-left={leftPanel}>
      <StyledRelativeContainer>
        <StyledArrow onClick={openLeftPanel} />
        <Switch>
          <Route path="/nickname" component={NicknameSide} />
          <Route path="/torrent" component={TorrentSide} />
        </Switch>
      </StyledRelativeContainer>
    </StyledRightSideBlock>
  );
});
