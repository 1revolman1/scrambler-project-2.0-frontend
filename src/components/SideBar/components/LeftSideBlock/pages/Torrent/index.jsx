import React, { useState, useRef } from "react";
import { StyledInput, StyledInputBlock, StyledButton } from "./styled";
import { useDispatch, useSelector } from "react-redux";
import { sidebarOpenLeftPanel } from "../../../../../../redux/actions/Sidebar";
import { torrentGetInfoAboutMyTorrents } from "../../../../../../redux/actions/Torrent";
import { dataForSidebar } from "../../../../../../redux/selector/Sidebar";
import { torrent_data } from "../../../../../../redux/selector/Torrent";

export default function Torrent() {
  //REDUX
  const windowSize = useRef(window && window.innerWidth > 1100 ? true : false);
  const dispatch = useDispatch();
  const sidebarData = useSelector(dataForSidebar);
  const torrentData = useSelector(torrent_data);
  //REDUX
  const [input, setInput] = useState("");
  const submitFunction = function (event) {
    event.preventDefault();
    if (
      new RegExp(
        "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
      ).test(input) &&
      input.split(".").length === 4
    ) {
      console.log("SENT", input);
      dispatch(torrentGetInfoAboutMyTorrents(input));
      dispatch(sidebarOpenLeftPanel(windowSize.current));
      setInput("");
    }
  };

  return (
    <>
      <h4>Поиск по конкретному IP:</h4>
      <form onSubmit={submitFunction}>
        <StyledInputBlock>
          <StyledInput
            onInput={(event) => setInput(event.target.value)}
            value={input}
            type="text"
            placeholder="Введите IP-адрес"
            minLength="7"
            maxLength="15"
            pattern="^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(.(?!$)|$)){4}$"
          />
        </StyledInputBlock>
        <StyledButton type="submit" value="Узнать про IP адрес" />
      </form>
      {sidebarData && torrentData && torrentData.ip !== sidebarData.ip && (
        <StyledButton
          onClick={() => {
            dispatch(torrentGetInfoAboutMyTorrents());
            dispatch(sidebarOpenLeftPanel(windowSize.current));
            setInput("");
          }}
          type="submit"
          value="Увидеть свои торренты"
        />
      )}
    </>
  );
}
