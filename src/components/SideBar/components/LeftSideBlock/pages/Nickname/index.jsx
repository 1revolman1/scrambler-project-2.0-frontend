import React, { useState, useRef } from "react";
import { StyledInput, StyledInputBlock, StyledButton } from "./styled";
import { useDispatch, useSelector } from "react-redux";
import { sidebarOpenLeftPanel } from "../../../../../../redux/actions/Sidebar";
import { nicknameGetInformationAboutNickname } from "../../../../../../redux/actions/Nickname";
import { isSelectedNicknameLoading } from "../../../../../../redux/selector/Nickname";

export default function Nickname() {
  //REDUX
  const windowSize = useRef(window && window.innerWidth > 1100 ? true : false);
  const dispatch = useDispatch();
  const isLoad = useSelector(isSelectedNicknameLoading);
  //REDUX
  const [input, setInput] = useState("");
  const submitFunction = function (event) {
    event.preventDefault();
    console.log("SENT", input);
    dispatch(nicknameGetInformationAboutNickname(input));
    dispatch(sidebarOpenLeftPanel(windowSize.current));
    setInput("");
  };

  return (
    <>
      <h4>Поиск по конкретному никнейму:</h4>
      <form onSubmit={submitFunction}>
        <StyledInputBlock>
          <StyledInput
            onInput={(event) => setInput(event.target.value)}
            value={input}
            type="text"
            placeholder="Введите никнейм"
          />
        </StyledInputBlock>
        {!isLoad && <StyledButton type="submit" value="Узнать про никнейм" />}
      </form>
    </>
  );
}
