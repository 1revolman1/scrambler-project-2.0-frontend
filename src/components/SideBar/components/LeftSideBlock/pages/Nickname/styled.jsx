import styled from "styled-components";

export const StyledInputBlock = styled.div`
  border: 2px solid #000000;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
  border-radius: 5px;
  margin: 20px 0;
`;

export const StyledInput = styled.input`
  width: 100%;
  border: none;
  outline: none;
  font-family: "Gilroy", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14.22px;
  line-height: 14px;
  text-align: left;
  letter-spacing: 0.015em;
  color: #949494;
  padding: 13px 8px;
`;
export const StyledButton = styled.input`
  margin: 5px 0;
  width: 100%;
  background: #ffffff;
  border: 1px solid #000000;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
  border-radius: 5px;
  padding: 12px 24px;
  transition: all 0.3s;
  cursor: pointer;
  /* p { */
  font-family: "Gilroy", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 15px;
  letter-spacing: 0.01em;
  color: #000000;
  /* } */
  &:focus,
  &:active {
    background: #0e4afb;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
    border-radius: 5px;
    /* p { */
    color: white;
    /* } */
  }
`;
