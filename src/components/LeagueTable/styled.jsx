import styled, { css } from "styled-components";

export const StyledPagination = styled.section`
  margin: 10px 0;
`;

export const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  border-style: hidden;
`;

export const StyledThead = styled.thead``;

export const StyledTbody = styled.tbody`
  &::before {
    content: "";
    display: block;
    height: 15px;
  }
`;

export const StyledTrTh = styled.tr``;

export const StyledTh = styled.th`
  padding: 12px 0 12px;
  background: black;
  font-family: "Gilroy", sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 13px;
  line-height: 26px;
  letter-spacing: 1px;
  color: #ffffff;
  text-transform: uppercase;
  max-width: 100px;
  @media screen and (max-width: 426px) {
    &[data-type="lastData"],
    &[data-type="size"] {
      display: none;
    }
  }
  &:first-of-type {
    border-top-left-radius: 9px;
    border-bottom-left-radius: 9px;
  }
  &:last-of-type {
    border-top-right-radius: 9px;
    border-bottom-right-radius: 9px;
  }
`;

export const StyledTrTd = styled.tr`
  border-bottom: 1px solid #bdbdbd;
  background: white;
  /* #e1a9a9 */
  &[data-type="Порно"] {
    background: #e1a9a9;
    * {
      color: white;
    }
  }
  &[data-type="Детское порно"] {
    background: red;
    * {
      color: white;
    }
  }
  ${({ clicked }) => {
    return css`
      &.row-number-${clicked} {
        background: var(--main-active-color);
        pointer-events: none;
        [type="PTS"] {
          span {
            background: white;
            color: black;
          }
        }
        * {
          color: #ffffff;
        }
      }
    `;
  }}
  &.clicked {
    background: var(--main-active-color);
    * {
      color: #ffffff;
    }
  }
  &:last-of-type {
    border-bottom: 0px solid #bdbdbd;
  }
`;

export const StyledTd = styled.td`
  text-align: center;
  padding: 10px;
  font-family: "Gilroy", sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: #000000;
  word-break: break-all;
  /* width: 100px; */
  max-width: 100px;

  @media screen and (max-width: 426px) {
    &[data-type="lastData"],
    &[data-type="size"] {
      display: none;
    }
  }
`;
