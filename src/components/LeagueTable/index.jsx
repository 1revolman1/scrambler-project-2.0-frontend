import React, { useState } from "react";
import { useTable, usePagination, useSortBy } from "react-table";
import {
  StyledTable,
  StyledThead,
  StyledTbody,
  StyledTh,
  StyledTd,
  StyledTrTh,
  StyledTrTd,
} from "./styled";

export default function TableContainer({
  columns,
  data,
  canSelectPlayer = false,
  getInformationAboutFilm = ({ id }) => console.log(id),
}) {
  const [clicked, setClicked] = useState(null);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page, // Instead of using 'rows', we'll use page,
    // which has only the rows for the active page

    // The rest of these things are super handy, too ;)
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    state: { pageIndex },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageIndex: 0,
        pageSize: 50,
        sortBy: [
          {
            id: "name",
            desc: false,
          },
        ],
      },
    },
    useSortBy,
    usePagination
  );

  return (
    <>
      <StyledTable {...getTableProps()}>
        <StyledThead>
          {headerGroups.map((headerGroup) => (
            <StyledTrTh {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => {
                return (
                  <StyledTh
                    data-type={column.id}
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.render("Header")}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </StyledTh>
                );
              })}
            </StyledTrTh>
          ))}
        </StyledThead>
        <StyledTbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <StyledTrTd
                className={`row-number-${row.id}`}
                data-type={row.original.type}
                clicked={clicked}
                {...row.getRowProps()}
              >
                {row.cells.map((cell) => {
                  return (
                    <StyledTd
                      onClick={() => {
                        if (!canSelectPlayer) return;
                        else if (
                          cell.row.original.type === "Фильм" ||
                          cell.row.original.type === "Series"
                        ) {
                          setClicked(row.id);
                          getInformationAboutFilm(cell.row.original);
                        }
                      }}
                      data-type={cell.column.id}
                      {...cell.getCellProps()}
                    >
                      {(() => {
                        if (cell.column.id === "lastData")
                          return new Date(cell.value).toLocaleString();
                        else return cell.render("Cell");
                      })()}
                    </StyledTd>
                  );
                })}
              </StyledTrTd>
            );
          })}
        </StyledTbody>
        <div className="pagination">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {"<<"}
          </button>
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {"<"}
          </button>
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {">"}
          </button>
          <button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            {">>"}
          </button>
          <span>
            Page
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>
          </span>
        </div>
      </StyledTable>
    </>
  );
}
