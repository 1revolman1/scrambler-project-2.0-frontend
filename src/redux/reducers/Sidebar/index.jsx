import { handleActions } from "../../helpers/immer";
import * as action from "../../actions/Sidebar";
const initialValue = {
  leftPanel: false,
  rightPanel: false,
  sideNav: false,
  data: {
    ip: null,
    country: null,
  },
  loading: true,
};

export const sidebar = handleActions(
  {
    [action.openRightPanel]: (draft, { payload }) => {
      draft.rightPanel = payload.view;
    },
    [action.openLeftPanel]: (draft, { payload }) => {
      draft.leftPanel = payload.view;
    },
    [action.openSideNavigation]: (draft, { payload }) => {
      draft.sideNav = payload.view;
    },
    //---------------GET DATA FOR SIDEBAR-----------------
    [action.getDataForSidebar]: (draft, { payload }) => {
      draft.loading = false;
      draft.data.ip = payload.ip;
      draft.data.country = payload.country;
    },
    [action.dataForSidebarLoaded]: (draft, { payload }) => {
      draft.loading = true;
    },
  },
  initialValue
);
