import { handleActions } from "../../helpers/immer";
import * as action from "../../actions/Torrent";
const initialValue = {
  data: null,
  selectedFilm: {
    data: null,
    id: null,
    loading: false,
  },
  loading: false,
};

export const torrent = handleActions(
  {
    [action.getInformationAboutMyTorrents]: (draft, { payload }) => {
      draft.data = payload.data;
      draft.loading = false;
    },
    [action.setLoadingStateForMyTorrents]: (draft) => {
      draft.loading = true;
    },
    //--------------FILM-------------------
    [action.selectFilm]: (draft, { payload }) => {
      draft.selectedFilm.id = payload.id;
    },
    [action.getInformationAboutRequestingFilmLoading]: (draft) => {
      draft.selectedFilm.loading = true;
    },
    [action.getInformationAboutRequestingFilm]: (draft, { payload }) => {
      draft.selectedFilm.data = payload.data;
      draft.selectedFilm.loading = false;
    },
  },
  initialValue
);
