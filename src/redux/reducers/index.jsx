import { combineReducers } from "redux";
import { home } from "./Home";
import { sidebar } from "./Sidebar";
import { torrent } from "./Torrent";
import { nickname } from "./Nickname";

export const rootReducer = combineReducers({
  home,
  sidebar,
  torrent,
  nickname,
});
