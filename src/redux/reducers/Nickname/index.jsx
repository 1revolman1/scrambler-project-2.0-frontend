import { handleActions } from "../../helpers/immer";
import * as action from "../../actions/Nickname";
const initialValue = {
  services: [],
  selectedNickname: {
    data: null,
    nickname: null,
    loading: false,
  },
  loading: false,
};
export const nickname = handleActions(
  {
    [action.getInformationAboutServicesLoading]: (draft) => {
      draft.loading = true;
    },
    [action.getInformationAboutServices]: (draft, { payload }) => {
      draft.services = payload.data;
      draft.loading = false;
    },
    [action.getInformationAboutNickname]: (draft, { payload }) => {
      draft.services = payload.data;
      draft.selectedNickname.nickname = payload.nick;
      draft.selectedNickname.loading = false;
    },
    [action.getInformationAboutNicknameLoading]: (draft, { payload }) => {
      draft.selectedNickname.loading = true;
    },
  },
  initialValue
);
