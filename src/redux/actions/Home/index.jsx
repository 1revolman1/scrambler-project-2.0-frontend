import { createAction } from "redux-act";
import { home, torrent_film } from "../../../service/links";

export const homedata = createAction("get home data");
export const homedata_loading = createAction("set loading state for home data");

export const homeGetHomeData = () => async (dispatch) => {
  dispatch(homedata_loading());
  let response = await fetch(home, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let data = await response.json();
    dispatch(homedata({ data }));
  }
};
export const selectFilm = createAction("select film by ID");
export const homeSelectFilmById = (id) => async (dispatch) => {
  dispatch(selectFilm({ id }));
};
export const getInformationAboutRequestingFilm = createAction(
  "get information about film from backend"
);
export const getInformationAboutRequestingFilmLoading = createAction(
  "set loading state for getting information about requesting film"
);
export const homeGetInformationAboutFilmById = (id) => async (dispatch) => {
  dispatch(getInformationAboutRequestingFilmLoading());
  let response = await fetch(torrent_film + `/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let data = await response.json();
    dispatch(getInformationAboutRequestingFilm({ data }));
  } else {
    const data = {
      photo: "",
      data: [
        {
          first: "Сообщение",
          second: "Про данный фильм нету информации",
        },
      ],
    };
    dispatch(getInformationAboutRequestingFilm({ data }));
  }
};
