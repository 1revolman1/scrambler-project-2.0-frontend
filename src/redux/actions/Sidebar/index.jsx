import { createAction } from "redux-act";
import { sidebar } from "../../../service/links";
export const openRightPanel = createAction("open right panel of sidebar");
export const openLeftPanel = createAction("open left panel of sidebar");
export const openSideNavigation = createAction(
  "open side navigation panel of sidebar"
);

export const sidebarOpenRightPanel = (view) => async (dispatch) => {
  dispatch(openRightPanel({ view }));
};
export const sidebarOpenLeftPanel = (view) => async (dispatch) => {
  dispatch(openLeftPanel({ view }));
};
export const sidebarOpenSideNavigation = (view) => async (dispatch) => {
  dispatch(openSideNavigation({ view }));
};
//----------------------------GET DATA FOR SIDEBAR--------------------------

export const getDataForSidebar = createAction(
  "get data for sidebar from backend"
);
export const dataForSidebarLoaded = createAction("set loading for sidebar");

export const sidebarGetDataForSideBar = () => async (dispatch) => {
  dispatch(dataForSidebarLoaded());
  let response1 = await fetch(sidebar, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
  });

  if (response1.status === 200) {
    let { ip, country } = await response1.json();
    dispatch(
      getDataForSidebar({
        ip,
        country,
      })
    );
  }
};
