import { createAction } from "redux-act";
import { nickname } from "../../../service/links";

export const getInformationAboutServices = createAction(
  "get information about services for nickname"
);
export const getInformationAboutServicesLoading = createAction(
  "set loading state for getting information about services for nickname"
);
export const nicknameGetInformationAboutServices = () => async (dispatch) => {
  dispatch(getInformationAboutServicesLoading());
  let response = await fetch(nickname, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let { data } = await response.json();
    dispatch(getInformationAboutServices({ data }));
  }
};
export const getInformationAboutNickname = createAction(
  "get information about services for nickname"
);
export const getInformationAboutNicknameLoading = createAction(
  "set loading state for getting information about services for nickname"
);
export const nicknameGetInformationAboutNickname = (nick) => async (
  dispatch,
  getState
) => {
  const {
    nickname: { services },
  } = getState();
  dispatch(getInformationAboutNicknameLoading());
  let response = await fetch(nickname + `/${nick}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let { data } = await response.json();
    data = data.map((elm, index) => {
      return {
        ...elm,
        ...services[index],
      };
    });
    dispatch(getInformationAboutNickname({ data, nick }));
  }
};
