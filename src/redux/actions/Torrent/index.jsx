import { createAction } from "redux-act";
import { torrent, torrent_film } from "../../../service/links";

export const getInformationAboutMyTorrents = createAction(
  "get information about torrent"
);
export const setLoadingStateForMyTorrents = createAction(
  "set loading state for torrents"
);
export const torrentGetInfoAboutMyTorrents = (ip = "") => async (dispatch) => {
  dispatch(setLoadingStateForMyTorrents());
  let response = await fetch(torrent + `/${ip}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let data = await response.json();
    dispatch(getInformationAboutMyTorrents({ data }));
  }
};

//-------------------------GET FILM DATA-------------------

export const selectFilm = createAction("select film by ID");
export const homeSelectFilmById = (id) => async (dispatch) => {
  dispatch(selectFilm({ id }));
};
export const getInformationAboutRequestingFilm = createAction(
  "get information about film from backend"
);
export const getInformationAboutRequestingFilmLoading = createAction(
  "set loading state for getting information about requesting film"
);
export const torrentGetInformationAboutFilmById = (id) => async (dispatch) => {
  dispatch(getInformationAboutRequestingFilmLoading());
  let response = await fetch(torrent_film + `/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    credentials: "same-origin",
  });
  if (response.status === 200) {
    let data = await response.json();
    dispatch(getInformationAboutRequestingFilm({ data }));
  } else {
    const data = {
      photo: "",
      data: [
        {
          first: "Сообщение",
          second: "Про данный фильм нету информации",
        },
      ],
    };
    dispatch(getInformationAboutRequestingFilm({ data }));
  }
};
