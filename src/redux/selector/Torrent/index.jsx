import { createSelector } from "reselect";

export const getTorrent = (state) => state.torrent;
export const getSelectedFilm = (state) => state.torrent.selectedFilm;

export const torrent_data = createSelector(getTorrent, ({ data }) => data);
export const isTorrentDataLoading = createSelector(
  getTorrent,
  ({ loading }) => loading
);

export const dataOfSelectedFilm = createSelector(
  getSelectedFilm,
  ({ data }) => data
);

export const selectedFilmId = createSelector(getSelectedFilm, ({ id }) => id);

export const isFilmLoaded = createSelector(
  getSelectedFilm,
  ({ loading }) => loading
);
