import { createSelector } from "reselect";

export const getHome = (state) => state.home;
export const getSelectedFilm = (state) => state.home.selectedFilm;

export const home_data = createSelector(getHome, ({ data }) => data);
export const isHomeDataLoading = createSelector(
  getHome,
  ({ loading }) => loading
);

export const dataOfSelectedFilm = createSelector(
  getSelectedFilm,
  ({ data }) => data
);

export const selectedFilmId = createSelector(getSelectedFilm, ({ id }) => id);

export const isFilmLoaded = createSelector(
  getSelectedFilm,
  ({ loading }) => loading
);
