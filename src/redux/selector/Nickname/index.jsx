import { createSelector } from "reselect";

export const getNickname = (state) => state.nickname;
export const getInfoAboutTypedNickname = (state) =>
  state.nickname.selectedNickname;

export const nicknameService = createSelector(
  getNickname,
  ({ services }) => services
);
export const isNicknameServiceLoading = createSelector(
  getNickname,
  ({ loading }) => loading
);
export const selectedNickname = createSelector(
  getInfoAboutTypedNickname,
  ({ data }) => data
);
export const isSelectedNicknameLoading = createSelector(
  getInfoAboutTypedNickname,
  ({ loading }) => loading
);
